#include <iostream> /* cout, printf */
#include <cstdlib>  /* srand, rand */
#include <ctime>    /* time */
#include <string>   /* string */
#include <vector>   /* vector */
#include <sstream>  /* istringstream */
#include <iterator> /* istream_iterator */

namespace strings
{
const char *SUM_OF_NUMS = "The sum of %d numbers is %d.";
const char *PROD_OF_NUMS = "The product of %d numbers is %d.";
const char *WHAT_ARE_NUMS = "What are these numbers?";
const char *CORRECT_ANSWER = "Your answer is correct.";
const char *WRONG_ANSWER = "Your answer is wrong.";
}; // namespace strings

inline void generateNumbers(int, int *, size_t);
inline void getNumbers(int *, size_t);
inline int getSum(int *, size_t);
inline int getProduct(int *, size_t);
inline std::vector<std::string> splitString(std::string);

int main(int argc, char const *argv[])
{
    std::srand(std::time(NULL));

    const int COUNT_NUMBERS = 3;
    int difficulty = 10;

    int numbers[COUNT_NUMBERS];

    do
    {
        generateNumbers(difficulty, numbers, COUNT_NUMBERS);
        difficulty++;

        int sum = getSum(numbers, COUNT_NUMBERS);
        std::printf(strings::SUM_OF_NUMS, COUNT_NUMBERS, sum);
        std::cout << std::endl;

        int product = getProduct(numbers, COUNT_NUMBERS);
        std::printf(strings::PROD_OF_NUMS, COUNT_NUMBERS, product);
        std::cout << std::endl;

        std::cout << strings::WHAT_ARE_NUMS << " ";
        std::cout << std::endl;
        getNumbers(numbers, COUNT_NUMBERS);

        if (getSum(numbers, COUNT_NUMBERS) == sum && getProduct(numbers, COUNT_NUMBERS) == product)
        {
            std::cout << strings::CORRECT_ANSWER;
            std::cout << std::endl;
        }
        else
        {
            std::cout << strings::WRONG_ANSWER;
            std::cout << std::endl;
            break;
        }
    } while (difficulty < 20);

    return 0;
}

void generateNumbers(int difficulty, int *numbers, size_t countNumbers)
{
    for (size_t i = 0; i < countNumbers; i++)
    {
        numbers[i] = rand() % difficulty + 1;
    }
}

void getNumbers(int *numbers, size_t countNumbers)
{
    std::string input;
    std::getline(std::cin, input);

    auto result = splitString(input);
    for (size_t i = 0; i < countNumbers; i++)
    {
        try
        {
            numbers[i] = std::stoi(result[i]);
        }
        catch (std::invalid_argument const &e)
        {
            std::cout << "Bad input: std::invalid_argument thrown" << std::endl;
        }
        catch (std::out_of_range const &e)
        {
            std::cout << "Integer overflow: std::out_of_range thrown" << std::endl;
        }
    }
}

int getSum(int *numbers, size_t countNumbers)
{
    int sum = 0;

    for (size_t i = 0; i < countNumbers; i++)
    {
        sum += numbers[i];
    }

    return sum;
}

int getProduct(int *numbers, size_t countNumbers)
{
    int product = 1;

    for (size_t i = 0; i < countNumbers; i++)
    {
        product *= numbers[i];
    }

    return product;
}

std::vector<std::string> splitString(std::string string)
{
    std::istringstream iss(string);
    return std::vector<std::string>(std::istream_iterator<std::string>{iss}, std::istream_iterator<std::string>());
}
